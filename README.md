# Test tabs as links

Cf <https://github.com/huntabyte/shadcn-svelte/discussions/889>

Problems:

- ctrl-click on tabs does not work (due to `preventDefault` in melt-ui)
- middle-click on tabs opens the tab in a new browser tab, but also changes the current tab
- the `<a>` link has `type="button"` set by melt-ui
